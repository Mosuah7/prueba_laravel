<?php

namespace App\Http\Controllers;

use App\Book;
use App\Category;
use Illuminate\Http\Request;
use Storage;
use File;
use Facades\App\Facades\AlertManager;
use Yajra\DataTables\Facades\DataTables;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('articles.book.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('articles.book.create')->with(['categories'=>Category::pluck('name','id')->toArray()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title'=>'required|min:3|max:50|string',
            'description'=>"required|min:3|max:50|string",
            'category_id'=>'required|numeric',
            'private'=>'required|numeric|in:1,2',
            'image'=>'required|file'
        ]);
        $file=$request->file('image');
        $namefile=time().".".$file->getClientOriginalExtension();
        Storage::disk('books')->put($namefile, File::get($file));
        $objBook = new Book($request->all());
        $objBook->image = $namefile;
        $objBook->user_id=\Auth::user()->id;
        $objBook->save();
        AlertManager::success('Registro guardado correctamente');
        return redirect()->route('articles.book.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\book  $book
     * @return \Illuminate\Http\Response
     */
    public function show(book $book)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\book  $book
     * @return \Illuminate\Http\Response
     */
    public function edit(book $book)
    {
        return view('articles.book.edit')->with([
            'book'=>$book,
            'categories'=>Category::pluck('name','id')->toArray()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\book  $book
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, book $book)
    {
        $this->validate($request,[
            'title'=>'required|min:3|max:50|string',
            'description'=>"required|min:3|max:50|string",
            'category_id'=>'required|numeric',
            'private'=>'required|numeric|in:1,2',
            'image'=>'required|file'
        ]);
        $file=$request->file('image');
        $namefile=time().".".$file->getClientOriginalExtension();
        Storage::disk('books')->put($namefile, File::get($file));
        //$book = new Book($request->all());
        $book->title = $request->title;
        $book->description = $request->description;
        $book->category_id = $request->category_id;
        $book->private = $request->private;
        $book->image = $namefile;
        $book->user_id=\Auth::user()->id;
        $book->save();
        AlertManager::success('Registro guardado correctamente');
        return redirect()->route('articles.book.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\book  $book
     * @return \Illuminate\Http\Response
     */
    public function destroy(book $book)
    {
        //
    }

    public function datatable(Request $request){
        if($request->ajax()){
        return Datatables::of(
                Book::where('user_id','=',\Auth::user()->id)
                ->select('id','title','description')->get()
            )->addColumn('actions',function($data){
                        return "
                        <a href='/articles/book/".$data->id."/edit' class='btn btn-warning btn-xs'>EDT</a>
                        <a href='/articles/book/show/".$data->id."' class='btn btn-primary btn-xs'>VER</a>
                        ";
                    }
            )->make(true);
        }else{
            return redirect()->route('articles.book.index');
        }
    }
}
