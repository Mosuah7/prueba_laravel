@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
        <div class="card">
                <div class="card-header">Editar Categoría</div>
                <div class="card-body">
                    {!! Form::open(['route'=>['admin.category.update',$category],'method'=>'PUT']) !!}
                    
                        @component('components.crudbasic')
                            @slot('name',$category->name)
                            @slot('description',$category->description)
                            @slot('parameters',[])
                        @endcomponent

                        {!! Form::submit('Guardar',['class'=>'btn btn-success btn-xs']) !!}
                        {{ link_to_route('admin.category.index','REGRESAR',[],['class'=>'btn btn-warning btn-xs']) }}

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection