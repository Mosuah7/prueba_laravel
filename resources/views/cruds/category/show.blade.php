@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
        <div class="card">
                <div class="card-header">Vista Categoría</div>
                <div class="card-body">

                    @component('components.crudbasic')
                        @slot('name',$category->name)
                        @slot('description',$category->description)
                        @slot('parameters',['readonly','readonly'])
                    @endcomponent
                
                        {{ link_to_route('admin.category.index','REGRESAR',[],['class'=>'btn btn-warning btn-xs']) }}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection