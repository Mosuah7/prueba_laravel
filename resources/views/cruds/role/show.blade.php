@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
        <div class="card">
                <div class="card-header">Vista de Roles</div>
                <div class="card-body">

                    @component('components.crudbasic')
                        @slot('name',$role->name)
                        @slot('description',$role->description)
                        @slot('parameters',['readonly','readonly'])
                    @endcomponent
                
                        {{ link_to_route('admin.role.index','REGRESAR',[],['class'=>'btn btn-warning btn-xs']) }}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection