@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
        <div class="card">
                <div class="card-header">Editar Rol</div>
                <div class="card-body">
                    {!! Form::open(['route'=>['admin.role.update',$role],'method'=>'PUT']) !!}
                    
                        @component('components.crudbasic')
                            @slot('name',$role->name)
                            @slot('description',$role->description)
                            @slot('parameters',[])
                        @endcomponent

                        {!! Form::submit('Guardar',['class'=>'btn btn-success btn-xs']) !!}
                        {{ link_to_route('admin.role.index','REGRESAR',[],['class'=>'btn btn-warning btn-xs']) }}

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection