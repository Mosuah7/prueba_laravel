@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
        <div class="card">
                <div class="card-header">Crear Libro</div>
                <div class="card-body">
                    {!! Form::open(['route'=>'articles.book.store','enctype'=>'multipart/form-data']) !!}

                        {!! Field::text('title',null,['placeholder'=>'Ingrese un título']) !!}
                        {!! Field::text('description',null,['placeholder'=>'Ingrese una descripción']) !!}
                        {!! Field::select('category_id',$categories,['empty'=>'**Seleccione**']) !!}
                        {!! Field::select('private',config('dataselect.confirmation'),['empty'=>'**Seleccione**']) !!}
                        {!! Field::file('image',['file','data-show-preview'=>true,'data-show-upload'=>false]) !!}

                        {!! Form::submit('Guardar',['class'=>'btn btn-success btn-xs']) !!}
                        {{ link_to_route('articles.book.index','REGRESAR',[],['class'=>'btn btn-warning btn-xs']) }}
                        
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection