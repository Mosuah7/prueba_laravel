@extends('layouts.app')

@section('content')
    
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
        <div class="card">
                <div class="card-header">Listado Libro</div>
                <div class="card-body">
                    <div class="row ">
                        <div class="col-md-8">
                            <a class="btn btn-primary btn-xs" href="{{ route('articles.book.create') }}">CREAR</a>
                        </div>
                    </div>
                    <table class="table" id="tbData">
                        <thead>
                            <th>Id</th>
                            <th>Título</th>
                            <th>Descripción</th>
                            <th>Acciones</th>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
    @section('datatable')
        <script src="/js/dtable.js"></script>
    @endsection