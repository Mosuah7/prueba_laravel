<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="alert alert-{{ session('alertmanager.type') }}" role="alert">
                {{ session('alertmanager.data') }}
            </div>
        </div>
    </div>
</div>