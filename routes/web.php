<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//tipos de rutas -- get,post,put,delete.

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//Pràctica de rutas, practica de get que devuelva hola mundo

Route::get('/saludos', function (){
    return "Hola Mundo";
});

Route::get('/mensaje/{nombre}/{apellido}/{id}', function($nombre,$apellido,$id){
    return "los datos ingresados: <br/>$nombre, <br/>$apellido, <br/>$id";
});

//ruta con parametros de vueltos y no devueltos
Route::get('/potencia/{base?}/{exponente?}', function($base=2,$exponente=2){
    return pow($base,$exponente);
});

//Filtrar rutas
Route::get('/numero/{id}', function($id){
    return pow($id, 2);
})->where(['id'=>'[0-9]+']);

Route::get('/numero/{id}/{id2}', function($id,$id2){
    return pow(3, $id);
})->where(['id'=>'[0-9]+','id2'=>'[5]+']);

Route::get('/slug/{accion}/{num}', function($accion,$num){
    switch ($accion) {
        case 'potencia':
            return pow(3, $num);
            break;
        
        case 'suma':
            return 4+$num;
            break;
    } 
})->where(['num'=>'[0-9]+','accion'=>'potencia|suma']);

Route::group(['prefix'=>'filtros'], function(){
    Route::get('/datos/{id}', function($id){
        return "numero $id";
    })->where(['id'=>'[0-9]+']);

    Route::get('/datos/{id}', function($id){
        return "Hola Mundo";
    })->where(['id'=>'hola']);

    Route::get('/datos/{id}', function($id){
        return "hola tu parametro fue: $id";
    })->where(['id'=>'[-\w]+'])->name('numero');
});

Route::get('/hora', function(){
    return date('Y-m-d H:i:s');
})->name('Hora');

Route::get('/saludo', 'PruebaController@getSaludo')->name('saludo');

Route::get('/saludo_concatenado_upper/{palabra}', 'PruebaController@getSaludo_concatenado_upper')->name('saludo');

//Resource para todo los mètodos

Route::resource('resource','ResourceController');

//Resource exclusivo
/*Route::resource('resource','ResourceController',['only'=>'index']);*/

//Resource todos menos uno
Route::resource('resource','ResourceController',['except'=>'index']);

Route::get('vista-simple','PruebaController@getVista')->name('vistasimple');

        /*                               *\
                INICIO DEL PROYECTO
        \*                               */

Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

//Route::group pertenecen a mismo grupo
Route::group(['middleware' => ['auth'],'prefix'=>'admin', 'as'=>'admin.'], function(){
    //atachar un controlador a un modelo especìfico
    Route::resource('role','Cruds\RoleController');
    Route::resource('category','Cruds\CategoryController');
});

Route::group(['middleware' => ['auth'],'prefix'=>'articles', 'as'=>'articles.'], function(){
    //atachar un controlador a un modelo especìfico
    Route::resource('book','BookController');
    Route::get('book-datatable','BookController@datatable');
});
