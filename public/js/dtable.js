$( function () {
    $.fn.dataTable.ext.errMode = 'throw';
    $('#tbData').DataTable({
        "processing":true,
        "serverSide":true,
        "deferRender":true,
        "ajax":"/articles/book-datatable",
          "columns":[
                {data:'id'},
                {data:'title'},
                {data:'description','render':function(data,type,row){
                    return $('<div />').html(row.description).text();
                }},
                {data:'actions',"bSortable":false,"searchable":false,"targets":0,
                'render':function(data,type,row){
                    return $('<div />').html(row.actions).text();
                }}
            ]
    });
} );